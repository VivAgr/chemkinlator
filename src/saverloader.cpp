/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "saverloader.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QMessageBox>
#include <iostream>

#include <unordered_map>

namespace SaverLoader {

using std::experimental::make_optional;
using std::experimental::nullopt;
using std::experimental::optional;
using std::get;
using std::make_pair;
using std::shared_ptr;
using std::string;
using std::tuple;
using std::unordered_map;

namespace {
  tuple<
    shared_ptr<ReactionDetails>,
    shared_ptr<SimulationDetails<double>>,
    shared_ptr<vector<QJsonObject>>,
    QStringList
  >
  fromJsonv0_1(const QJsonObject &simulationDetails) {
    // Loading REACTIONS
    QJsonObject reactionsObject = simulationDetails.value("reactions").toObject();
    QMap<int, QString> reactions_input{};
    QStringList species;

    for(QString reactionNumber : reactionsObject.keys()) {
      int keyInt = QStringRef( &reactionNumber, 1, reactionNumber.length()-1 ).toInt();
      reactions_input.insert( keyInt, reactionsObject.value(reactionNumber).toString() );
    }

    // reactions_input.values returns all reactions ordered in their Ri number
    auto reactionDetails = std::make_shared<ReactionDetails>( reactions_input.values() );
    auto reactions_ = reactionDetails->getReactions(); // careful, the reactions order could change when processing

    // Loading CONCENTRATIONS
    QJsonObject concentrationsObject = simulationDetails.value("concentrations").toObject();
    for(QString aSpecies : concentrationsObject.keys()) {
      double value = concentrationsObject.value(aSpecies).toDouble();
      reactionDetails->setInitialConcentration( aSpecies, value );
      species << aSpecies;
    }

    // Loading RATES
    QJsonObject ratesObject = simulationDetails.value("rates").toObject();
    for(QString reactionNumber : ratesObject.keys()) {
      int     keyInt       = QStringRef( &reactionNumber, 1, reactionNumber.length()-1 ).toInt() - 1;
      QString reactionName = reactions_[keyInt];

      double  value        = ratesObject.value(reactionNumber).toDouble();

      reactionDetails->setRate( reactionName, value );
      //qDebug() << reactionName << value;
    }

    //double T_0   = simulationDetails.value("T_0").toDouble();
    double Tmax  = simulationDetails.value("Tmax").toDouble();
    double Delta = simulationDetails.value("Delta").toDouble();
    double ATOL  = simulationDetails.value("ATOL").toDouble();
    double RTOL  = simulationDetails.value("RTOL").toDouble();

    auto simuDetails = std::make_shared<SimulationDetails<double>>(Tmax, Delta, ATOL, RTOL);

    auto tabs = std::make_shared<vector<QJsonObject>>();
    QJsonObject standardTab = {
      {"type", "timeseries"},
      {"name", "Time Series"}
    };
    tabs->emplace_back( standardTab );

    return std::make_tuple(reactionDetails, simuDetails, tabs, species);
  }

  tuple<
    shared_ptr<ReactionDetails>,
    shared_ptr<SimulationDetails<double>>,
    shared_ptr<vector<QJsonObject>>,
    QStringList
  >
  fromJsonv0_2(const QJsonObject &simuJsonObject) {
    // TODO: show window with parsing error if some QJsonValue can't be converted into object, array, or double
    // Loading REACTIONS
    QJsonObject networkObject        = simuJsonObject.value("network").toObject();
    QJsonObject concentrationsObject = networkObject.value("concentrations").toObject();
    QJsonObject ratesObject          = networkObject.value("rates").toObject();

    QStringList species;

    QStringList reactions = ratesObject.keys();
    auto reactionDetails = std::make_shared<ReactionDetails>( reactions );

    // Loading CONCENTRATIONS
    for(const QString& aSpecies : concentrationsObject.keys()) {
      double value = concentrationsObject.value(aSpecies).toDouble();
      reactionDetails->setInitialConcentration( aSpecies, value );
      species << aSpecies;
    }

    // Loading RATES
    for(const QString& reaction : reactions) {
      double value = ratesObject.value(reaction).toDouble();
      reactionDetails->setRate( reaction, value );
    }

    QJsonObject simuDetailsObject = simuJsonObject.value("simulation details").toObject();
    //double T_0   = simuDetailsObject.value("T_0").toDouble();
    double Tmax  = simuDetailsObject.value("Tmax").toDouble();
    double Delta = simuDetailsObject.value("Delta").toDouble();
    double ATOL  = simuDetailsObject.value("ATOL").toDouble();
    double RTOL  = simuDetailsObject.value("RTOL").toDouble();

    auto simuDetails = std::make_shared<SimulationDetails<double>>(Tmax, Delta, ATOL, RTOL);

    QJsonArray tabsArray = simuJsonObject.value("tabs").toArray();
    auto tabs = std::make_shared<vector<QJsonObject>>();
    for (const QJsonValue& tabValue : tabsArray) {
      QJsonObject tabObject = tabValue.toObject();
      tabs->emplace_back( tabObject );
    }

    return std::make_tuple(reactionDetails, simuDetails, tabs, species);
  }

  tuple<
    shared_ptr<ReactionDetails>,
    shared_ptr<SimulationDetails<double>>,
    shared_ptr<vector<QJsonObject>>,
    QStringList
  >
  fromJsonv0_3(const QJsonObject &simuJsonObject) {
    auto&& tup = fromJsonv0_2(simuJsonObject);
    QStringList species;

    for (auto&& aSpecies : simuJsonObject.value("species to plot").toArray()) {
      species << aSpecies.toString();
    }

    return std::make_tuple(get<0>(tup), get<1>(tup), get<2>(tup), species);
  }

} // namespace

optional<tuple<
  shared_ptr<ReactionDetails>,
  shared_ptr<SimulationDetails<double>>,
  shared_ptr<vector<QJsonObject>>,
  QStringList
>>
fromJson(const QString &json_file) {

  QJsonDocument json = QJsonDocument::fromJson(json_file.toUtf8());

  if( ! json.isObject() ) {
    QMessageBox msgBox;
    msgBox.setText("Something is wrong with the selected file. "
                   "Please make sure it isn't broken or malformed!");
    msgBox.exec();
    return nullopt;
  } else {
    QJsonObject simuDetails = json.object();

    QJsonValue version_ = simuDetails.value("version");
    if( version_.type() == QJsonValue::Double ) {
      double version = version_.toDouble();

      if( version == 0.1 ) {
        return make_optional( fromJsonv0_1(simuDetails) );
      } else if( version == 0.2 ) {
        return make_optional( fromJsonv0_2(simuDetails) );
      } else if( version == 0.3 ) {
        return make_optional( fromJsonv0_3(simuDetails) );
      }

    }

    // any other version
    QMessageBox msgBox;
    msgBox.setText("The reaction network couldn't be loaded. "
                   "The file (.simu.json) is in a format version not supported. "
                   "If you have the lastest CHEMKINLATOR, then the file may be corrupted");
    msgBox.exec();
    return nullopt;
  }
}

QByteArray
toJson(const ReactionDetails &reactionDetails,
       const SimulationDetails<double> &simuDetails,
       const vector<QJsonObject> &tabsInfo,
       const QStringList & speciesToPlot) {

  // saving SIMULATION DETAILS
  QJsonObject simuDetailsObject {
    {"T_0"  , 0  },
    {"Tmax" , simuDetails.getTmax() },
    {"Delta", simuDetails.getDelta()},
    {"ATOL" , simuDetails.getATOL() },
    {"RTOL" , simuDetails.getRTOL() },
  };

  // saving NETWORK (concentrations and rates)
  const QMap<QString, double>&
    initialConcentrations = reactionDetails.getInitialConcentrations();
  QJsonObject concentrationsObject;
  for (const QString& aSpecies: reactionDetails.getSpecies()) {
    concentrationsObject.insert( aSpecies, initialConcentrations.value(aSpecies) );
  }

  const QMap<QString, double>& reactionsRates = reactionDetails.getReactionsRates();
  QJsonObject ratesObject;
  for (const QString& reaction : reactionDetails.getReactions()) {
    ratesObject.insert( reaction, reactionsRates.value(reaction) );
  }

  QJsonObject networkObject {
    {"concentrations", concentrationsObject},
    {"rates", ratesObject}
  };

  // saving TABS
  QJsonArray tabsArray;
  for (const QJsonObject& tabObject : tabsInfo) {
    tabsArray << tabObject;
  }

  // saving species to plot
  QJsonArray speciesArray;
  for (auto&& aSpecies : speciesToPlot) {
    speciesArray << aSpecies;
  }

  QJsonObject simuJsonObject {
    {"_WARNING!_", "This file was generated using CHEMKINLATOR. Any change to the file could make it unreadable, modify it at your own risk."},
    {"network", networkObject},
    {"simulation details", simuDetailsObject},
    {"tabs", tabsArray},
    {"species to plot", speciesArray},
    {"version", 0.3 }
  };

  return QJsonDocument(simuJsonObject).toJson(QJsonDocument::Indented);
}

tuple<shared_ptr<ReactionDetails>,
      shared_ptr<SimulationDetails<double>>,
      shared_ptr<vector<QJsonObject>>,
      QStringList>
defaultReactionsAndSimulationDetls()
{
  // Creating default Set of Reactions.
  // This is the same file as the one found in `examples/Kondepudi-Nelson.simu.json`
  QString defaultSimulation_json = R"(
{
    "network": {
        "concentrations": {
            "x": 1,
            "y": 1
        },
        "rates": {
            "-> x": 1,
            "2 x + y -> 3 x": 1,
            "x ->": 1,
            "x -> y": 1.8
        }
    },
    "simulation details": {
        "ATOL": 1e-15,
        "Delta": 0.04,
        "RTOL": 1e-15,
        "T_0": 0,
        "Tmax": 200
    },
    "species to plot": [
        "x",
        "y"
    ],
    "tabs": [
        {
            "name": "Time Series",
            "type": "timeseries"
        },
        {
            "label for x axis": "x -> y",
            "name": "Bifurcation",
            "number of experiments": 100,
            "rates": {
                "x -> y": {
                    "from": 1.5,
                    "to": 2.5
                }
            },
            "species to plot": [
                "x",
                "y"
            ],
            "total final steps": 25,
            "type": "bifurcation",
            "use first species to plot": true
        }
    ],
    "version": 0.3
}
)";

  QJsonDocument json = QJsonDocument::fromJson(defaultSimulation_json.toUtf8());

  return fromJsonv0_2(json.object());
}

}  // namespace SaverLoader
