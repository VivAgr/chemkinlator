/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_MAINWINDOWSTATE_H_
#define SRC_MAINWINDOWSTATE_H_

#include <QCheckBox>
#include <QLineEdit>
#include <QMap>
#include <QString>
#include <QStringList>

#include <memory>
#include <unordered_map>

#include "simulationdetails.h"

/*!
 * @brief Simple class that saves the current state of the main window, it allows the
 *        some modifications on the main window through a couple of methods too
 */
class MainWindowState {
  using SimuDetailsBoxes = SimulationDetails<std::shared_ptr<QLineEdit>>;
  using speciesReactionsBoxes = std::unordered_map<std::string, std::shared_ptr<QLineEdit>>;

  QStringList species;
  QStringList reactions;
  std::shared_ptr<speciesReactionsBoxes> speciesBoxes;     //!< References to the boxes in the mainwindow
  std::shared_ptr<speciesReactionsBoxes> reactionsBoxes;   //!< References to the boxes in the mainwindow
  std::shared_ptr<SimuDetailsBoxes>      simuDetailsBoxes; //!< References to the boxes in the mainwindow
  std::vector<std::shared_ptr<QCheckBox>> speciesToPlotCheckBoxes;

public:
  explicit MainWindowState(
      QStringList               species,
      QStringList               reactions,
      std::shared_ptr<speciesReactionsBoxes>  speciesBoxes,
      std::shared_ptr<speciesReactionsBoxes>  reactionsBoxes,
      std::shared_ptr<SimuDetailsBoxes>       simuDetailsBoxes,
      std::vector<std::shared_ptr<QCheckBox>> speciesToPlotCheckBoxes
  );

  MainWindowState(const MainWindowState&) = default;
  MainWindowState(MainWindowState&&) = default;
  MainWindowState& operator=(const MainWindowState&) = default;
  MainWindowState& operator=(MainWindowState&&) = default;
  ~MainWindowState() = default;

  const QStringList& getSpecies()   { return species; }
  const QStringList& getReactions() { return reactions; }
  double getRate(const QString& reaction);
  double getConcentration(const QString& a_species);

  /*!
   * @brief Enables all boxes from the left column, sometimes a tab can disable a box
   *        to show the user that that particular reaction or species value is overwitten
   *        by the tab configuration
   */
  void enableAllBoxes();
  void setSpeciesToPlotEnabled     (bool b); //!< Sets a species enable status (either enable or disable)
  void setSpeciesEnabled           (const std::string &a_species, bool b); //!< Sets a species enable status (either enable or disable)
  void setReactionsEnabled         (const std::string &reaction, bool b); //!< Sets a reaction enable status (either enable or disable)
  void setSimulatorVariableEnabled (const std::string &var,      bool b); //!< Sets a simulation detail enable status (either enable or disable)
  bool isSpeciesEnabled            (const std::string &species ); //!< Determines the enable status of a species
  bool isReactionsEnabled          (const std::string &reaction); //!< Determines the enable status of a reaction
  bool isSimulatorVariableEnabled  (const std::string &var     ); //!< Determines the enable status of a simulation detail
  QStringList getSpeciesToPlot() const;
};

#endif // SRC_MAINWINDOWSTATE_H_
