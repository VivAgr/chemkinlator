/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#include <QDebug>

#include "tabs/timeseriestab.h"

#include <QDebug>
#include <QFileDialog>
#include <QGridLayout>
#include <QMessageBox>
#include <QTextStream>

#include <qwt_legend.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_renderer.h>
#include <qwt_symbol.h>

#include <iostream>

#include "qt_version_compatibility.h"
#include "simulators/time_series.h"

using std::shared_ptr;
using std::unique_ptr;

TimeSeriesTab::TimeSeriesTab(unique_ptr<MainWindowState> mwstate, QWidget *parent)
    : MainWindowTab(std::move(mwstate), parent),
      saveDataAction(std::make_shared<QAction>(this)),
      exportPlotAction(std::make_shared<QAction>(this)),
      qwt_plot (std::make_unique<QwtPlot>())
{

  this->setMinimumSize(630, 430);

  //QwtPlot* qwt_plot = new QwtPlot();
  auto tablayout = new QGridLayout(this);  // Creating layout for this Tab
  tablayout->addWidget(qwt_plot.get(), 0, 0);

  qwt_plot->setTitle("Species Concentrations");
  qwt_plot->setCanvasBackground(Qt::white);
  qwt_plot->insertLegend(new QwtLegend());
  qwt_plot->setAxisTitle(QwtPlot::yLeft, "Concentrations");
  qwt_plot->setAxisTitle(QwtPlot::xBottom, "Time");

  QwtPlotGrid *grid = new QwtPlotGrid();
  grid->setMajorPen(Qt::gray, 0, Qt::DotLine);
  grid->setMinorPen(Qt::darkGray, 0, Qt::DotLine);
  grid->attach(qwt_plot.get());

  saveDataAction->setText("Save Data");
  exportPlotAction->setText("Export Plot");

  // disabling buttons to save data and plot
  saveDataAction->setEnabled(false);
  exportPlotAction->setEnabled(false);
  executed = false;

  actions.emplace_back(saveDataAction);
  actions.emplace_back(exportPlotAction);

  connect(saveDataAction.get(), SIGNAL(triggered(bool)), this, SLOT(when_save_data_is_pushed(bool)));
  connect(exportPlotAction.get(), &QAction::triggered,
      [this](bool){
        QwtPlotRenderer renderer;
        QSizeF filesize = {
          static_cast<qreal>(this->qwt_plot->width())/3.6,
          static_cast<qreal>(this->qwt_plot->height())/3.6
        };
        renderer.exportTo(this->qwt_plot.get(), "plot.pdf", filesize, 300);
      }
  );
}

//TimeSeriesTab::~TimeSeriesTab() {
//  std::cout << "Cleaning TimeSeriesTab" << std::endl;
//}

void
TimeSeriesTab::execute(const ReactionDetails &rd,
                     const SimulationDetails<double> &simulationDetails) {

  TimeSeries simulator(rd, simulationDetails);
  runresult = simulator.run();

  //std::cout << "Inside execute. Time intervals " << run.getNumTimeIntervals() << std::endl;

  saveDataAction->setEnabled(true);
  exportPlotAction->setEnabled(true);
  executed = true;

  auto species = runresult->getSpecies();
  auto concents = runresult->getConcentrationsThruTime();
  auto timelabel = concents->at("Time");

  concentrations.clear();
  for (auto&& aSpecies : species) {
    QPolygonF points;
    int i = 0;
    for (auto&& conc: concents->at(aSpecies)) {
      points << QPointF(timelabel[i], conc);
      //qDebug() << timelabel[i] << " " << conc;
      i++;
    }

    concentrations.insert(QString::fromStdString(aSpecies), points);
  }

  replot();
}

bool
TimeSeriesTab::isStillRunning() {
  return false;
}

void
TimeSeriesTab::cleanTab() {
  curves.clear();
  concentrations.clear();
  saveDataAction->setEnabled(false);
  exportPlotAction->setEnabled(false);
  executed = false;
}

void
TimeSeriesTab::when_save_data_is_pushed(bool checked) {
  qInfo() << "Action triggered: Data Save";

  QString fileName;

  QFileDialog dialog(this);
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  dialog.setFileMode(QFileDialog::AnyFile);
  dialog.setDirectory(QDir::homePath());
  dialog.setNameFilter(tr("Text File (*.txt)"));
  dialog.exec();

  if (dialog.selectedFiles().isEmpty()) return;

  fileName = dialog.selectedFiles().first();

  if (QFile::exists(fileName)) QFile::remove(fileName);

  QFile dataFile(fileName);
  if (dataFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
    QTextStream stream(&dataFile);
    //std::cout << runresult->toPlainText();
    stream << QString::fromStdString(runresult->toPlainText());
    dataFile.close();
  } else {
    QMessageBox::warning(
        this,
        "Failure saving file",
        "The file cannot be saved. "
        "Check whether you have the rights to access and modify the file.",
        QMessageBox::Ok,
        QMessageBox::NoButton
    );
  }
}

void
TimeSeriesTab::replot() {
  if (!executed) return;

  std::array<Qt::GlobalColor, 5> colors = {
    Qt::blue, Qt::red, Qt::green, Qt::magenta, Qt::gray
  };

  // Cleaning curves from plot. When a curve is deleted from memory it deletes
  // itself from the plot
  curves.clear();

  int j = 0;
  for (auto&& aSpecies : mwstate->getSpeciesToPlot()) {
    auto curve = std::make_unique<QwtPlotCurve>();
    curve->setTitle(aSpecies);
    curve->setPen(colors[j%5], 1),
    curve->setRenderHint(QwtPlotItem::RenderAntialiased, true);
    curve->setSamples(concentrations[aSpecies]);
    curve->attach(qwt_plot.get());

    curves.emplace(aSpecies, std::move(curve));
    j++;
  }

  qwt_plot->replot();
}

void
TimeSeriesTab::reapplyBoxesState() {}

QJsonObject
TimeSeriesTab::toJsonObject() {
  return QJsonObject {
    {"type", "timeseries"}
  };
}
