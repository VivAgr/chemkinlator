/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#include <QDebug>

#include "tabs/bifurcationtab.h"

#include <QDebug>
#include <QFileDialog>
#include <QJsonArray>
#include <QMessageBox>
#include <QTextStream>
#include <QThreadPool>

#include <qwt_legend.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_renderer.h>
#include <qwt_scale_engine.h>
#include <qwt_symbol.h>

#include <fstream>
#include <iostream>
#include <utility>

#include "simulators/time_series.h"

using std::experimental::optional;
using std::make_shared;
using std::make_unique;
using std::make_pair;
using std::pair;
using std::string;
using std::unique_ptr;
using std::vector;

BifurcationTab::BifurcationTab(unique_ptr<MainWindowState> mwstate, QWidget *parent, const optional<QJsonObject> &tabInfo_)
    : MainWindowTab(std::move(mwstate), parent),
      //ui_btab (std::shared_ptr<Ui::BifurcationTab>(new Ui::BifurcationTab(), [](Ui::BifurcationTab* ptr){ std::cout << "Cleaning Ui::BifurcationTab" << std::endl; delete ptr; })),
      doneExecutingThread(true),
      saveDataAction(make_shared<QAction>(this)),
      exportPlotAction(make_shared<QAction>(this)),
      qwt_plot(std::make_unique<QwtPlot>()),
      mythreadpool(new QThreadPool(this))
{

  //qRegisterMetaType<ExecutionDataBifurcationTab>("ExecutionDataBifurcationTab");

  // Setting up UI
  ui_btab.setupUi(this);
  //int qscroll_height = ui_btab.scrollable_column_widget->size().width();
  //ui_btab.scrollable_column_widget->resize(240, qscroll_height);

  ui_btab.splitter->addWidget(qwt_plot.get());

  qwt_plot->setTitle("Bifurcation Graph");
  qwt_plot->setCanvasBackground(Qt::white);
  //qwt_plot->setAxisScale(QwtPlot::yLeft, 0.0, 10.0);
  qwt_plot->insertLegend(new QwtLegend());
  qwt_plot->setAxisTitle(QwtPlot::yLeft, "Concentrations");
  qwt_plot->setAxisTitle(QwtPlot::yRight, "Temperature");
  qwt_plot->setAxisTitle(QwtPlot::xBottom, "Time");

  QwtPlotGrid *grid = new QwtPlotGrid();
  grid->setMajorPen(Qt::gray, 0, Qt::DotLine);
  grid->setMinorPen(Qt::darkGray, 0, Qt::DotLine);
  grid->attach(qwt_plot.get());
  // End of UI setting

  QStringList selected_rates;
  QStringList selected_species;
  // Detecting if there is extra info to fill the interface
  if( auto tabInfo = tabInfo_ ) {
    int num_exps    = static_cast<int>( tabInfo->value("number of experiments").toDouble() );
    int final_steps = static_cast<int>( tabInfo->value("total final steps").toDouble() );
    ui_btab.number_of_experiments_box->setText( QString("%1").arg( num_exps ) );
    ui_btab.total_final_steps_box    ->setText( QString("%1").arg( final_steps ) );

    selected_rates = tabInfo->value("rates").toObject().keys();
    for (const QJsonValue & a_speciesValue : tabInfo->value("species to plot").toArray()) {
      selected_species << a_speciesValue.toString();
    }
    //qDebug() << selected_species;
    bool axis_check = tabInfo->value("use first species to plot").toBool();
    ui_btab.labelForXAxis->setCurrentIndex(axis_check);
  }

  saveDataAction->setText("Save Data");
  exportPlotAction->setText("Export Plot");
  saveDataAction->setEnabled(false); // disabling buttons to save data and plot
  exportPlotAction->setEnabled(false);
  executed = false;

  actions.emplace_back(saveDataAction);
  actions.emplace_back(exportPlotAction);

  connect(saveDataAction.get(), SIGNAL(triggered(bool)), this, SLOT(when_save_data_is_pushed(bool)));
  connect(exportPlotAction.get(), &QAction::triggered,
      [this](bool){
        QwtPlotRenderer renderer;
        QSizeF filesize = {
          static_cast<qreal>(this->qwt_plot->width())/3.6,
          static_cast<qreal>(this->qwt_plot->height())/3.6
        };
        renderer.exportTo(this->qwt_plot.get(), "plot.pdf", filesize, 300);
      }
  );

  ui_btab.labelForXAxis->addItem("Number of Experiments");
  QString labelForXAxis;
  if(tabInfo_ && !tabInfo_->value("label for x axis").isUndefined()) {
    labelForXAxis = tabInfo_->value("label for x axis").toString();
  }

  // Adding reactions to chose from to reaction_selected_box
  QStringList reactions = this->mwstate->getReactions();
  int j = 1;
  for (int i = 0; i < reactions.size(); i++) {
    QString reaction = reactions.at(i);
    if ( tabInfo_ && selected_rates.contains(reaction) ) {
      selectedRates.emplace_back(reaction.toStdString());
      QJsonObject reactionObject = tabInfo_->value("rates").toObject().value(reaction).toObject();
      double startingPoint = reactionObject.value("from").toDouble();
      double endingPoint   = reactionObject.value("to").toDouble();
      addRateBoxesAndLabels(reaction, startingPoint, endingPoint);

      ui_btab.labelForXAxis->addItem(reaction);
      if(!labelForXAxis.isEmpty() && reaction == labelForXAxis) {
        ui_btab.labelForXAxis->setCurrentIndex(j);
      }
      j++;
    } else {
      ui_btab.reaction_selected_box->addItem(reaction);
    }
  }
  connect(ui_btab.labelForXAxis, SIGNAL(currentIndexChanged(int)), this, SLOT(replot()));

  // Setting restrictions to what the user can enter into each box
  qintvalidator.setBottom(1);
  qdoublevalidator.setRange(0e0, std::numeric_limits<double>::max(), 16);

  QStringList species = this->mwstate->getSpecies();
  for (int i = 0; i < species.size(); i++) {
    QString a_species = species.at(i);
    auto speciesBox = make_shared<QCheckBox>(a_species);
    speciesToPlotCheckBoxes.push_back(speciesBox);
    ui_btab.selectSpeciesLayout->addWidget( speciesBox.get() );

    // setting to plot first two species
    if ( tabInfo_ && selected_species.contains(a_species) ) {
      speciesBox->setChecked(true);
    } else if(!tabInfo_ && i<2) {
      speciesBox->setChecked(true);
    }

    connect(speciesBox.get(), SIGNAL(stateChanged(int)), parent, SLOT(modelModified()));
    connect(speciesBox.get(), SIGNAL(stateChanged(int)), this, SLOT(replot()));
  }

  /*
   *ui_btab.rate_step_size_box->setValidator(&qdoublevalidator);
   *ui_btab.initial_rate_box->setValidator(&qdoublevalidator);
   */
  ui_btab.number_of_experiments_box->setValidator(&qintvalidator);
  ui_btab.total_final_steps_box->setValidator(&qintvalidator);

  connect(ui_btab.addRateButton, SIGNAL(clicked()),
          this, SLOT(when_add_rate_is_clicked()));

  // Connecting boxes to when they are modified to WindowIsModified
  connect(ui_btab.number_of_experiments_box, SIGNAL(textEdited(QString)),
          parent, SLOT(modelModified()));
  connect(ui_btab.total_final_steps_box, SIGNAL(textEdited(QString)),
          parent, SLOT(modelModified()));
  connect(ui_btab.addRateButton, SIGNAL(clicked()),
          parent, SLOT(modelModified()));
  connect(ui_btab.labelForXAxis, SIGNAL(currentIndexChanged(int)),
          parent, SLOT(modelModified()));

}

BifurcationTab::~BifurcationTab() {
  //std::cout << "Cleaning BifurcationTab" << std::endl;
  if(executingThread) {
    executingThread->setParentDeleted();
    mythreadpool->waitForDone();
  }
}

void
BifurcationTab::execute(const ReactionDetails &rd,
                        const SimulationDetails<double> &simuDetails) {

  if(!doneExecutingThread) {
    std::cerr << "Trying to run but thread is still working" << std::endl;
    return;
  }

  if(selectedRates.empty()) {
    QMessageBox::information(
        this,
        "Nothing to do",
        "No rate has been selected to plot against.\n"
        "Please, select a reaction and press \"Add rate\".",
        QMessageBox::Ok
    );
    return;
  } // don't do anything if there are no rates to modify
  // TODO: add an alert telling the user to add a rate

  // Getting variables from the interface
  auto numberOfExperiments = static_cast<unsigned int>(ui_btab.number_of_experiments_box->text().toInt()); // e.g., => 100
  int totalFinalSteps = ui_btab.total_final_steps_box->text().toInt();     // e.g., => 80
  vector<string> selectedRates_(selectedRates);

  auto initialAndIntervalRates = getChangesForRates();

  vector<double> startingPointRates = std::get<0>( initialAndIntervalRates );
  vector<double> intervalRates      = std::get<1>( initialAndIntervalRates );

  // Getting which species are to be plotted
  QStringList speciesToPlot = getSpeciesToPlot();
  // checking if the x axis should show the values of the first reaction
  //bool useFirstRateForXAxis = getUseFirstRateForXAxis(startingPointRates, intervalRates);

  doneExecutingThread = false;

  executingThread = make_unique<BifurcationExecuteRunnable>(this,
      totalFinalSteps, selectedRates_, startingPointRates, intervalRates,
      speciesToPlot, rd, simuDetails, numberOfExperiments);
  executingThread->setAutoDelete(false);
  mythreadpool->start(executingThread.get());
}

bool
BifurcationTab::isStillRunning() {
  return !doneExecutingThread;
}

void
BifurcationTab::cleanTab() {
  curves.clear();
  saveDataAction->setEnabled(false);
  exportPlotAction->setEnabled(false);
  executed = false;
}

void
BifurcationTab::reapplyBoxesState() {
  for(const string &rate : selectedRates) {
    mwstate->setReactionsEnabled(rate, false);
  }
  mwstate->setSpeciesToPlotEnabled(false);
}

QStringList
BifurcationTab::getSpeciesToPlot() {
  QStringList speciesToPlot;
  for (auto&& aSpeciesBox : speciesToPlotCheckBoxes) {
    if(aSpeciesBox->isChecked()) {
      speciesToPlot << aSpeciesBox->text();
    }
  }
  return speciesToPlot;
}

pair<vector<double>, vector<double>>
BifurcationTab::getChangesForRates() {
  vector<double> startingPointRates
               , intervalRates;

  for(const string &rate : selectedRates) {
    // supposing that the user hasn't changed any value of selectedRatesBoxes
    // TODO(helq): Capture signal when one of the boxes from selectedRatesBoxes is changed so it deactivates replot
    startingPointRates.emplace_back( selectedRatesBoxes[rate].first->text().toDouble()  );
    double endingRate = selectedRatesBoxes[rate].second->text().toDouble();
    int numberOfExperiments;

    numberOfExperiments = ui_btab.number_of_experiments_box->text().toInt();
    intervalRates.emplace_back( (endingRate - startingPointRates.back()) / (numberOfExperiments-1) );
    //std::cout << rate << " " << (endingRate - startingPointRates.back()) / (numberOfExperiments-1) << std::endl;
  }
  return make_pair(startingPointRates, intervalRates);
}

void
BifurcationTab::replot() {
  if (!executed) return;

  std::array<Qt::GlobalColor, 5> colors = {
    Qt::blue, Qt::red, Qt::green, Qt::magenta, Qt::gray
  };

  // Cleaning curves from plot. When a curve is deleted from memory it deletes
  // itself from the plot
  curves.clear();

  // Determining X Label
  int     currentIndex   = ui_btab.labelForXAxis->currentIndex();
  QString reaction_label = ui_btab.labelForXAxis->itemText(currentIndex);

  auto concentrations = runresult.run->getConcentrationsThruTime();
  auto species = getSpeciesToPlot();

  std::vector<double> runlabel;
  auto isLabelInConcents = concentrations->find(reaction_label.toStdString());
  if (isLabelInConcents == concentrations->end()) {
    reaction_label = "Number of Experiments";
    runlabel = concentrations->at("Run");
  } else {
    runlabel = concentrations->at(reaction_label.toStdString());
  }

  int j = 0;
  for (auto&& aSpecies : species) {
    QPolygonF points;

    auto speciesConc = concentrations->at(aSpecies.toStdString());
    for (int i = 0; i<speciesConc.size(); i++) {
      points << QPointF(runlabel[i], speciesConc[i]);
      //qDebug() << runlabel[i] << " " << conc;
    }

    auto curve = std::make_unique<QwtPlotCurve>();
    curve->setTitle(aSpecies);
    curve->setStyle(QwtPlotCurve::Dots);
    curve->setPen(colors[j%5], 3),
    curve->setRenderHint(QwtPlotItem::RenderAntialiased, true);
    curve->setSamples(points);
    curve->attach(qwt_plot.get());

    curves.emplace(aSpecies, std::move(curve));
    j++;
  }

  qwt_plot->setAxisTitle(QwtPlot::xBottom, reaction_label);
  qwt_plot->setAxisScale(QwtPlot::xBottom, runlabel[0], runlabel[runlabel.size()-1]);

  // Finally. Replot
  qwt_plot->replot();
}

void
BifurcationTab::when_add_rate_is_clicked() {
  if(ui_btab.reaction_selected_box->count() > 0) {

    // getting index and text
    int     currentIndex = ui_btab.reaction_selected_box->currentIndex();
    QString indexTextQt  = ui_btab.reaction_selected_box->itemText(currentIndex);

    // adding to list of rates and deleting from combo box
    selectedRates.emplace_back(indexTextQt.toStdString());
    ui_btab.reaction_selected_box->removeItem(currentIndex);

    double rateForIndex = mwstate->getRate(indexTextQt);
    double startingPoint = rateForIndex/2;
    double endingPoint   = startingPoint*3;

    addRateBoxesAndLabels(indexTextQt, startingPoint, endingPoint);
    ui_btab.labelForXAxis->addItem(indexTextQt);

    // updating state of rates shown on the principal column
    mwstate->enableAllBoxes();
    reapplyBoxesState();

    // if there are no reactions left to add, then disable button
    if(ui_btab.reaction_selected_box->count() == 0) {
      // there aren't any reactions/rates left to add
      ui_btab.addRateButtonWidget->setEnabled(false);
    }

    executed = false;
    set_value_progress_bar(0);
  }
}

void
BifurcationTab::addRateBoxesAndLabels(QString indexTextQt, double startingPoint, double endingPoint) {
  string indexText = indexTextQt.toStdString();

  // Adding boxes with info about the added rate
  auto rateLabel        = make_shared<QLabel>(indexTextQt, this); // this is bad, but for some freaking reason Qt is in charge of cleaning all objects
  auto startingPointBox = make_shared<QLineEdit>(this);
  auto endingPointBox   = make_shared<QLineEdit>(this);
  selectedRatesLabels.emplace_back( rateLabel );
  selectedRatesBoxes.emplace( indexText, make_pair(startingPointBox, endingPointBox) );

  int lastPos = ui_btab.ratesLayout->rowCount();
  ui_btab.ratesLayout->addWidget(rateLabel.get(), lastPos, 0, 1, 2);
  ui_btab.ratesLayout->addWidget(startingPointBox.get(), lastPos+1, 0);
  ui_btab.ratesLayout->addWidget(endingPointBox.get(), lastPos+1, 1);

  QString startingPointStr = QString::number(startingPoint, 'g', 16);
  QString endingPointStr   = QString::number(endingPoint, 'g', 16);

  // setting values in boxes
  startingPointBox->setValidator(&qdoublevalidator);
  startingPointBox->setPlaceholderText(startingPointStr);
  startingPointBox->setText(startingPointStr);

  endingPointBox->setValidator(&qdoublevalidator);
  endingPointBox->setPlaceholderText(endingPointStr);
  endingPointBox->setText(endingPointStr);

  connect(startingPointBox.get(), SIGNAL(textEdited(QString)),
          parent(), SLOT(modelModified()));
  connect(endingPointBox.get(), SIGNAL(textEdited(QString)),
          parent(), SLOT(modelModified()));
}

void
BifurcationTab::set_value_progress_bar(int value) {
  ui_btab.progressBar->setValue(value);
}

void
BifurcationTab::when_save_data_is_pushed(bool checked) {
  qInfo() << "[Bifurcation tab]: Action triggered: Data Save";

  QString fileName;

  QFileDialog dialog(this);
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  dialog.setFileMode(QFileDialog::AnyFile);
  dialog.setDirectory(QDir::homePath());
  dialog.setNameFilter(tr("Text File (*.txt)"));
  dialog.exec();

  if (dialog.selectedFiles().isEmpty()) return;

  fileName = dialog.selectedFiles().first();

  if (QFile::exists(fileName)) QFile::remove(fileName);

  QFile dataFile(fileName);
  if (dataFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
    QTextStream stream(&dataFile);
    //std::cout << runresult.run->toPlainText();
    stream << QString::fromStdString(runresult.run->toPlainText());
    dataFile.close();
  } else {
    QMessageBox::warning(
        this,
        "Failure saving file",
        "The file cannot be saved. "
        "Check whether you have the rights to access and modify the file.",
        QMessageBox::Ok,
        QMessageBox::NoButton
    );
  }
}

QJsonObject
BifurcationTab::toJsonObject() {
  QJsonObject ratesObject;
  for(const string &rate : selectedRates) {
    double startingPoint = selectedRatesBoxes[rate].first->text().toDouble();
    double endingPoint   = selectedRatesBoxes[rate].second->text().toDouble();
    QJsonObject rateValues {
      {"from", startingPoint},
      {"to",   endingPoint}
    };
    ratesObject.insert( QString::fromStdString(rate), rateValues );
  }

  QJsonArray speciesArray;
  for (const QString& aSpecies : getSpeciesToPlot()) {
    speciesArray << aSpecies;
  }

  int i = ui_btab.labelForXAxis->currentIndex();
  QString labelForXAxis = ui_btab.labelForXAxis->itemText(i);

  return QJsonObject {
    {"type", "bifurcation"},
    {"rates", ratesObject},
    {"number of experiments", static_cast<double>(ui_btab.number_of_experiments_box->text().toInt()) },
    {"total final steps",     static_cast<double>(ui_btab.total_final_steps_box->text().toInt()) },
    {"use first species to plot", i != 0},
    {"label for x axis", labelForXAxis},
    {"species to plot", speciesArray}
  };
}

BifurcationExecuteRunnable::BifurcationExecuteRunnable(
  BifurcationTab* tab,
  int totalFinalSteps,
  std::vector<std::string> selectedRates,
  std::vector<double> startingPointRates,
  std::vector<double> intervalRates,
  QStringList speciesToPlot,
  const ReactionDetails &rd,
  const SimulationDetails<double> &simuDetails,
  unsigned int numberOfExperiments
)
  : tab                 (tab),
    totalFinalSteps     (totalFinalSteps),
    selectedRates       (selectedRates),
    startingPointRates  (startingPointRates),
    intervalRates       (intervalRates),
    speciesToPlot       (speciesToPlot),
    rd                  (rd),
    simuDetails         (simuDetails),
    numberOfExperiments (numberOfExperiments)
{
  parentDeleted = false;
}

void
BifurcationExecuteRunnable::run() {
  ReactionDetails rd_(rd);

  // variable to save results of all ran experiments
  vector<pair<vector<double>, SimulationResults>> runs;

  QStringList species   = rd_.getSpecies();

  TimeSeries simulator(rd_, simuDetails);
  for (std::size_t i = 0; i < numberOfExperiments; i++) {
    //std::cout << "." << std::flush;
    auto totalExps     = static_cast<double>(numberOfExperiments);
    auto progressValue = static_cast<int>(100 * i / totalExps);
    vector<double> runId = {static_cast<double>(i) };

    mutex.lock();
    if(parentDeleted) {
      mutex.unlock();
      return;
    }
    // setting something in the interface shouldn't be done in different thread, it may fail
    // the best thing to do is to call a method that is in the principal thread to repaint
    QMetaObject::invokeMethod(tab, "set_value_progress_bar", Q_ARG(int, progressValue));
    mutex.unlock();

    // Setting new values for experiment
    for(unsigned j = 0; j < selectedRates.size(); j+=1) {
      const string &rate = selectedRates[j];
      double new_rate = startingPointRates[j] + intervalRates[j] * i;
      rd_.setRate(QString::fromStdString(rate), new_rate);
      //std::cout << rate << " " << new_rate << std::endl;
      runId.emplace_back( new_rate );
    }

    // running experiment and taking last 100 timeSteps from it
    auto run = simulator.run(SimulatorDebug::Off)->sliceEnd(totalFinalSteps);

    runs.emplace_back( std::move(runId), run );
  }
  //std::cout << std::endl;

  vector<string> newCols = { "Run" };
  for (auto&& col : selectedRates) {
    newCols.emplace_back( col );
  }

  QStringList selectedRatesQStringList;
  for (auto&& rate : selectedRates) {
    selectedRatesQStringList.append(QString::fromStdString(rate));
  }

  mutex.lock();
  if(parentDeleted) {
    mutex.unlock();
    return;
  }
  // Saving data that the plot will use to create the graph
  tab->runresult.run = std::make_unique<SimulationResults>(newCols, runs);
  tab->runresult.reactions            = selectedRatesQStringList;
  tab->runresult.Tmax                 = simuDetails.getTmax();
  tab->runresult.startX               = startingPointRates;
  tab->runresult.numberOfExperiments  = numberOfExperiments;

  tab->runresult.endX.clear();
  for (int i = 0; i < startingPointRates.size(); i++) {
    tab->runresult.endX.emplace_back(startingPointRates[i] + intervalRates[i]*(numberOfExperiments-1));
  }

  // Showing that computation has been finished
  QMetaObject::invokeMethod(tab, "set_value_progress_bar", Q_ARG(int, 100));

  tab->saveDataAction->setEnabled(true); // careful, this may generate a horrible failure, TODO: consider other options
  tab->exportPlotAction->setEnabled(true);
  tab->executed = true;
  tab->doneExecutingThread = true;

  QMetaObject::invokeMethod(tab, "replot");
  mutex.unlock();
}

void
BifurcationExecuteRunnable::setParentDeleted() {
  mutex.lock();
  parentDeleted = true;
  mutex.unlock();
}
