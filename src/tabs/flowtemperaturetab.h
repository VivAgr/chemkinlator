/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_TABS_FLOWTEMPERATURETAB_H_
#define SRC_TABS_FLOWTEMPERATURETAB_H_

#include <QDoubleValidator>
#include <QIntValidator>
#include <QJsonObject>
#include <QLineEdit>
#include <QTemporaryFile>

#include <qwt_plot.h>
#include <qwt_plot_curve.h>

#include <memory>
#include <string>
#include <vector>

#include "optional.hpp"

#include "reactiondetails.h"
#include "simulationresults.h"
#include "tabs/mainwindowtab.h"
#include "ui_flowtemperaturetab.h"

class FlowTemperatureTab : public MainWindowTab {
  Q_OBJECT

public:
  explicit FlowTemperatureTab(
      std::unique_ptr<MainWindowState> mwstate,
      QWidget *parent = Q_NULLPTR,
      const std::experimental::optional<QJsonObject> & = std::experimental::nullopt);
  ~FlowTemperatureTab();

  void execute(const ReactionDetails &, const SimulationDetails<double> &);
  bool isStillRunning();
  void cleanTab();
  void reapplyBoxesState();
  QJsonObject toJsonObject();

public slots:
  void replot();

private slots:
  void when_save_data_is_pushed(bool);

private:
  bool executed;
  /*!
   * @brief General structure of the ui, Ui::TemperatureTab is automatically generated
   *        from `src/tabs/ui/tempertaturetab.ui`
   */
  Ui::FlowTemperatureTab ui_temptab;

  QDoubleValidator qdoublevalidator; //!< Double validator used to restrict what is entered in some boxes
  QDoubleValidator qdoublevalidator_no_min; //!< Double validator used to restrict what is entered in some boxes

  //std::vector<std::string> selectedRates;
  std::map<QString, QLineEdit*> flowEntrySpecies;
  std::map<QString, QLineEdit*> concentrationInputSpecies;
  std::map<QString, QLineEdit*> activationEnergies;
  std::map<QString, QLineEdit*> preExpFactors;
  std::map<QString, QLineEdit*> reactionsEnthalpy;

  /*!
   * @brief Action to be shown in the menu "Tab". When pressed it calls when_save_data_is_pushed
   */
  std::shared_ptr<QAction> saveDataAction;
  std::shared_ptr<QAction> exportPlotAction;

  std::unique_ptr<SimulationResults> runresult;

  std::unique_ptr<QwtPlot> qwt_plot;

  QMap<QString, QPolygonF> concentrations;

  std::map<QString, std::unique_ptr<QwtPlotCurve>> curves;
};

#endif  // SRC_TABS_FLOWTEMPERATURETAB_H_
