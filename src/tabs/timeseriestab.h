/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_TABS_TIMESERIESTAB_H_
#define SRC_TABS_TIMESERIESTAB_H_

#include <QAction>
#include <QTemporaryFile>

#include <qwt_plot.h>
#include <qwt_plot_curve.h>

#include "reactiondetails.h"
#include "simulationresults.h"
#include "tabs/mainwindowtab.h"

/*!
 * @brief Tab that simulates a reaction network with the parameters specified by the user
 *        in the main left column, not less no more
 */
class TimeSeriesTab : public MainWindowTab {
  Q_OBJECT

public:
  /*!
   * @brief Creates a new TimeSeriesTab from a unique_ptr to a MainWindowState and a parent
   *        widget
   *
   * @param mwstate A MainWindowState pointer which gives access to modify and consult the
   *                state of the interface
   * @param parent  A QWidget
   */
  explicit TimeSeriesTab(std::unique_ptr<MainWindowState> mwstate, QWidget *parent = Q_NULLPTR);

  ~TimeSeriesTab() = default;

  void execute(const ReactionDetails &, const SimulationDetails<double> &);
  bool isStillRunning();
  void cleanTab();
  void reapplyBoxesState();
  QJsonObject toJsonObject();

public slots:

  void replot();

private slots:
  /*!
   * @brief Shows to the user a dialog to save the `dataFile` when "Tab -> Save Data"
   *        option is pressed
   *
   * @param checked This parameter is ignored, it is necessary to connect this slot with the
   *                signal finished from the button
   */
  void when_save_data_is_pushed(bool);

private:
  bool executed;
  /*!
   * @brief Action that triggers the slot when_save_data_is_pushed, it is automatically
   *        loaded to the "Tab" menu if the current tab is displayed.
   */
  std::shared_ptr<QAction> saveDataAction;
  std::shared_ptr<QAction> exportPlotAction;

  std::unique_ptr<SimulationResults> runresult;

  std::unique_ptr<QwtPlot> qwt_plot;

  QMap<QString, QPolygonF> concentrations;

  std::map<QString, std::unique_ptr<QwtPlotCurve>> curves;
};

#endif  // SRC_TABS_TIMESERIESTAB_H_
