/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mainwindowstate.h"

#include <QDebug>

#include <iostream>
#include <utility>

using std::shared_ptr;
using std::string;
using std::unordered_map;
using std::vector;
using speciesReactionsBoxes = unordered_map<string, shared_ptr<QLineEdit>>;

MainWindowState::MainWindowState(
    QStringList               species_,
    QStringList               reactions_,
    shared_ptr<speciesReactionsBoxes> speciesBoxes_,
    shared_ptr<speciesReactionsBoxes> reactionsBoxes_,
    shared_ptr<SimuDetailsBoxes>      simuDetailsBoxes_,
    vector<shared_ptr<QCheckBox>>     speciesToPlotCheckBoxes_
    )
    : species          ( std::move( species_          ) ),
      reactions        ( std::move( reactions_        ) ),
      speciesBoxes     ( std::move( speciesBoxes_     ) ),
      reactionsBoxes   ( std::move( reactionsBoxes_   ) ),
      simuDetailsBoxes ( std::move( simuDetailsBoxes_ ) ),
      speciesToPlotCheckBoxes ( speciesToPlotCheckBoxes_ ) {}

//MainWindowState::~MainWindowState() {
//  std::cout << "Cleaning MainWindowState" << std::endl;
//}

void
MainWindowState::setSpeciesToPlotEnabled(bool b) {
  for (auto&& checkbox : speciesToPlotCheckBoxes) {
    checkbox->setEnabled(b);
  }
}

void
MainWindowState::setSpeciesEnabled(const string &a_species, bool b) {
  if (speciesBoxes->count(a_species) > 0) {
    speciesBoxes->at(a_species)->setEnabled(b);
  } else {
    std::cerr << "Species:" << a_species << "is not in the list of species";
  }
}

void
MainWindowState::setReactionsEnabled(const string &reaction, bool b) {
  if (reactionsBoxes->count(reaction) > 0) {
    reactionsBoxes->at(reaction)->setEnabled(b);
  } else {
    std::cerr << "Reaction:" << reaction << "is not in the list of reactions";
  }
}

void
MainWindowState::setSimulatorVariableEnabled(const string &var, bool b) {
  if         (var == "Tmax" ) { simuDetailsBoxes->getTmax() ->setEnabled(b);
  } else { if(var == "Delta") { simuDetailsBoxes->getDelta()->setEnabled(b);
  } else { if(var == "ATOL" ) { simuDetailsBoxes->getATOL() ->setEnabled(b);
  } else { if(var == "RTOL" ) { simuDetailsBoxes->getRTOL() ->setEnabled(b);
  } else {
    std::cerr << "Variable:" << var << "is not in the list of variables for ";
  }}}}
}

bool
MainWindowState::isSpeciesEnabled(const string &species) {
  if (speciesBoxes->count(species) > 0) {
    return speciesBoxes->at(species)->isEnabled();
  } else {
    std::cerr << "Species:" << species << "is not in the list of species";
    return false; // change for std::optional or equivalent
  }
}

bool
MainWindowState::isReactionsEnabled(const string &reaction) {
  if(reactionsBoxes->count(reaction) > 0) {
    return reactionsBoxes->at(reaction)->isEnabled();
  } else {
    std::cerr << "Reaction:" << reaction << "is not in the list of reactions";
    return false;
  }
}

bool
MainWindowState::isSimulatorVariableEnabled(const string &var) {
  if         (var == "Tmax" ) { return simuDetailsBoxes->getTmax() ->isEnabled();
  } else { if(var == "Delta") { return simuDetailsBoxes->getDelta()->isEnabled();
  } else { if(var == "ATOL" ) { return simuDetailsBoxes->getATOL() ->isEnabled();
  } else { if(var == "RTOL" ) { return simuDetailsBoxes->getRTOL() ->isEnabled();
  } else {
    std::cerr << "Variable:" << var << "is not in the list of variables for ";
    return false;
  }}}}
}

void
MainWindowState::enableAllBoxes() {
  for( auto aSpeciesBox : *speciesBoxes ) {
    aSpeciesBox.second->setEnabled(true);
  }
  for( auto reactionBox : *reactionsBoxes ) {
    reactionBox.second->setEnabled(true);
  }
  for (auto&& checkbox : speciesToPlotCheckBoxes) {
    checkbox->setEnabled(true);
  }

  simuDetailsBoxes->getTmax() ->setEnabled(true);
  simuDetailsBoxes->getDelta()->setEnabled(true);
  simuDetailsBoxes->getATOL() ->setEnabled(true);
  simuDetailsBoxes->getRTOL() ->setEnabled(true);
}

double
MainWindowState::getRate(const QString& reaction) {
  shared_ptr<QLineEdit> rateLineEdit = reactionsBoxes->at(reaction.toStdString());
  return rateLineEdit->text().toDouble();
}

double
MainWindowState::getConcentration(const QString& a_species) {
  shared_ptr<QLineEdit> a_speciesLineEdit = speciesBoxes->at(a_species.toStdString());
  return a_speciesLineEdit->text().toDouble();
}

QStringList
MainWindowState::getSpeciesToPlot() const {
  QStringList lst;
  for (auto&& checkbox : speciesToPlotCheckBoxes) {
    if(checkbox->isChecked())
      lst << checkbox->text();
  }
  return lst;
}
