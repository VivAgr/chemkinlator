!> @brief Contains the code in charge of executing a simulation of a reaction network
module chemical_simulator_with_temp
  implicit none

  !! All these variables are accessible and used by the subroutine F
  integer :: &
    n_reactions, & !< Number of reactions in the reactions network
    n_species      !< Number of reactions in the reactions network
  double precision :: &
    Temp_ambient, &      !< Ambient temperature
    Temp_reactives, &    !< Temperature at which the reactives enter
    Volume_reactor, &    !< Volume of reactor
    Flow_output, &       !< Total output flow
    kappa, &             !< Kappa variable
    heat_capacity
  double precision, dimension(:), pointer :: &
    flow_entry_for_species, &  !< Flow that enters the reactor
    concentration_entry_for_species, &  !< Concentration for each species
    activation_energy, &     !< Activation energy for each reaction
    reactions_enthalpy, &     !< Reaction Enthalpy for each reaction
    preexponential_factor    !< Preexponential factor for each reaction
  integer, dimension(:,:), pointer :: &
    stoichiometric_matrix, &
    reactions_order_matrix

  interface
    !> Interface definition for DLSODE, the original code is from the 60's and 70's, thus
    !! it lacks the explicit declaration of the intentions for each variable passed to its
    !! multiple subroutines.
    !!
    !> From the original documentation: 
    !!
    !! Livermore solver for ordinary differential equations. DLSODE solves the initial-value
    !! problem for stiff or nonstiff systems of first-order ODE's,
    !!
    !! dy/dt = f(t,y),   or, in component form,
    !!
    !! dy(i)/dt = f(i) = f(i,t,y(1),y(2),...,y(N)),  i=1,...,N. 
    !!
    !! For detailed info look into the source code
    !! 
    !! @param        F      Name of subroutine for right-hand-side vector f. This name must be declared
    !!                      EXTERNAL in calling program.
    !! @param[in]    NEQ    Number of first-order ODE's.
    !! @param[inout] Y      Array of values of the y(t) vector, of length NEQ.
    !! @param[inout] T      Value of the independent variable. On return it will be the current value
    !!                      of t (normally TOUT).
    !! @param[in]    TOUT   Next point where output is desired (.NE. T).
    !! @param[in]    ITOL   1 or 2 according as ATOL (below) is a scalar or an array.
    !! @param[in]    RTOL   Relative tolerance parameter (scalar).
    !! @param[in]    ATOL   Absolute tolerance parameter (scalar or array).
    !! @param[in]    ITASK  Flag indicating the task DLSODE is to perform.
    !! @param[inout] ISTATE Index used for input and output to specify the state of the calculation.
    !! @param[in]    IOPT   Flag indicating whether optional inputs are used: 0 (No) and 1 (Yes).
    !! @param[inout] RWORK  Real work array of length at least:
    !! @param[in]    LRW    Declared length of RWORK (in user's DIMENSION statement).
    !! @param[inout] IWORK  Integer work array of length at least:
    !! @param[in]    LIW    Declared length of IWORK (in user's DIMENSION statement).
    !! @param JAC           Name of subroutine for Jacobian matrix (MF = 21 or 24). If used, this name
    !!                      must be declared EXTERNAL in calling program. If not used, pass a dummy name.
    !! @param[in]    MF     Method flag. Standard values are:
    !!                       10 for Nonstiff (Adams) method, no Jacobian used;
    !!                       21 for Stiff (BDF) method, user-supplied full Jacobian;
    !!                       22 for Stiff method, internally generated full Jacobian;
    !!                       24 for Stiff method, user-supplied banded Jacobian; and
    !!                       25 for Stiff method, internally generated banded Jacobian.
    subroutine DLSODE (F, NEQ, Y, T, TOUT, ITOL, RTOL, ATOL, ITASK, &
                    & ISTATE, IOPT, RWORK, LRW, IWORK, LIW, JAC, MF)
      implicit none
      external                                        :: F, JAC
      integer, intent(in)                             :: NEQ, ITOL, ITASK, IOPT, LRW, LIW, MF
      integer, intent(inout)                          :: ISTATE
      double precision, dimension(NEQ), intent(inout) :: Y
      double precision, intent(inout)                 :: T
      double precision, intent(in)                    :: TOUT, RTOL
      double precision, dimension(*), intent(in)      :: ATOL
      double precision, dimension(LRW), intent(inout) :: RWORK
      integer, dimension(LIW), intent(inout)          :: IWORK
    end subroutine DLSODE
  end interface

  contains

  subroutine run_simulation(n_species_in, n_reactions_in, &
                            activation_energy_in, preexponential_factor_in, concentrations, &
                            stoichiometric_matrix_in, reactions_order_matrix_in, &
                            flow_entry_for_species_in, concentration_entry_for_species_in, &
                            Temp_ambient_in, Temp_reactives_in, Volume_reactor_in, kappa_in, &
                            reactions_enthalpy_in, heat_capacity_in, &
                            T_0, Tmax, &
                            Delta, ATOL, RTOL, &
                            num_time_intervals, concentrations_through_time, &
                            output_to_stdout, failure_flag)
    ! getting environment defined stdin, stdout, and stderr
    use, intrinsic :: iso_fortran_env, only : stdin=>input_unit, &   ! default is 5
                                              stdout=>output_unit, & ! default is 6
                                              stderr=>error_unit     ! default is 0

    implicit none

    integer, intent(in) :: &
      n_species_in, n_reactions_in
    double precision, dimension(n_species_in+1), intent(inout) :: &
      concentrations
    double precision, intent(in) :: &
      T_0, Tmax, Delta, RTOL, Temp_ambient_in, Temp_reactives_in, Volume_reactor_in, kappa_in, &
      heat_capacity_in
    double precision, dimension(n_species_in), intent(in), target :: &
      flow_entry_for_species_in, concentration_entry_for_species_in
    double precision, dimension(n_reactions_in), intent(in), target :: &
      activation_energy_in, &     !< activation energy for each reaction
      preexponential_factor_in, & !< preexponential factor for each reaction
      reactions_enthalpy_in        !< reaction enthalpy for each reaction
    double precision, dimension(1), intent(in) :: &
      ATOL

    !> Number of total expected executions of DLSODE
    integer, intent(inout) :: &
      num_time_intervals
    !> Saves concentration of each species at each instant on time, i.e., each time it saves the
    !! result of each executing of DLSODE
    double precision, dimension(num_time_intervals, n_species_in+2), intent(inout) :: &
      concentrations_through_time
    integer, dimension(n_species_in, n_reactions_in), intent(in), target :: &
      stoichiometric_matrix_in, &
      reactions_order_matrix_in

    ! careful this can be a source of mistakes, logical(kind=1) is meant to be
    ! that the logical (boolean) variable is stored in a 8bit cell
    !> boolean setted to .true. if one wants to see the whole output of running the subroutine
    logical(kind=1), intent(in) :: output_to_stdout
    !> If it is setted to .true. then something went terribly wrong in the computation.
    !> This shouldn't happen, look at source code for more info
    logical(kind=1), intent(out) :: failure_flag

    double precision                  :: T, TOUT, Hmax
    integer                           :: ITOL, ITASK, ISTATE, IOPT, LRW, LIW, MF, LPR, Max_step
    double precision, dimension(1500) :: RWORK
    integer, dimension(100)           :: IWORK

    integer :: i, j, k

    ! This flag is defined on "chemkinlator.pro" qmake passes this flag only in windows
#if WINDOWS_FLAG
    character(len=*), parameter :: nullfile="nul"
#else
    ! GNU fortran doesn't define a variable to indicate linux as the platform being compiled
    ! see: https://stackoverflow.com/a/33813775
    character(len=*), parameter :: nullfile="/dev/null"
#endif

    integer                     :: logfile

    ! deciding if to print to stdout or discard output (send it to "/dev/null")
    if (output_to_stdout) then
      logfile = stdout
    else
      logfile = 24
      open(unit=logfile, file=nullfile)
    end if

    flow_entry_for_species => flow_entry_for_species_in
    concentration_entry_for_species => concentration_entry_for_species_in
    activation_energy => activation_energy_in
    preexponential_factor => preexponential_factor_in
    stoichiometric_matrix => stoichiometric_matrix_in
    reactions_order_matrix => reactions_order_matrix_in
    reactions_enthalpy => reactions_enthalpy_in

    n_reactions  = n_reactions_in
    n_species    = n_species_in
    failure_flag = .false.
    t            = t_0     ! copying starting time
    Temp_ambient = Temp_ambient_in
    Temp_reactives = Temp_reactives_in
    Volume_reactor = Volume_reactor_in
    kappa = kappa_in
    heat_capacity = heat_capacity_in
    Flow_output = sum(flow_entry_for_species_in(:))

    !write (logfile,*) 'concentrations(',i,') =', concentrations(i)

    !**************** Parámetros DLSODE *************************************
    Hmax     = 10D0       ! Maximun step size
    Max_step = 10000000D0
    TOUT     = 0D0
    ITOL     = 1
    ITASK    = 1
    ISTATE   = 1
    IOPT     = 1
    LRW      = 1500
    LIW      = 100
    MF       = 22         ! DLSODE suministra el jacobiano full. MF = 10 Sistemas no rígidos.
    do lpr=5,10 ! Input options
      RWORK(lpr)=0
      IWORK(lpr)=0
    end do
    !RWORK(5) = H !h0
    RWORK(6) = Hmax
    IWORK(6) = Max_step

    !****************** DLSODE integration cycle *******************
    j = 1
    do while (Tout <= Tmax)
      call DLSODE(F,n_species+1,concentrations,T,TOUT,ITOL,RTOL,ATOL,ITASK,ISTATE,IOPT,RWORK,LRW,IWORK,LIW,Jac,MF)
      
      do i=1,n_species
        if (concentrations(i) <= 0) then
          concentrations(i)=0D0
          write (logfile,*) 'Careful, this shouldn''t happen!' ! TODO: ensure nothing bad happens when defaulting this variable to zero
          write (logfile,*) 'Reactant/Species ran out'
          write (logfile,*) 'concentrations(',i,') =', concentrations(i)
          !call sleep(1)
        end if
      end do

      ! debug information, to prevent printing set output_to_stdout to .false.
      write(logfile,"(35(1PE13.5))") T, (concentrations(k), k=1, (n_species+1))

      ! saving current concentration into the array
      concentrations_through_time(j,1) = T ! saving time
      concentrations_through_time(j,2:) = concentrations(:) ! saving concentrations on time T

      if (ISTATE < 0) then
        write(stderr,"(///22H ERROR HALT.. ISTATE =,I3)") ISTATE
        write(stderr,*) 'ERROR while computing, please take a look at dlsode.for (line 128) for more info and report the bug'
        failure_flag = .true.
        return
      end if

      TOUT = TOUT + Delta
      j = j + 1
    end do
    num_time_intervals = j-1 ! this SHOULD be equal to `(Tmax-T_0)/Delta`,
                             ! if it doesn't then, there is probably a round error somewhere

    !************ (end) DLSODE integration cycle *******************
    write(logfile,"(/12H NO. STEPS =,I4,11H  NO. F-S =,I4,11H  NO. J-S =,I4)") IWORK(11), IWORK(12), IWORK(13)

    write (logfile,*) 'END OF SIMULATION EXECUTION'
  end subroutine run_simulation

  !****************************************************************************
  !> "DUMMY" subroutine, necessary for DLSOVE
  subroutine JAC
  end subroutine JAC

  !*****************************************************************************
  !> Subroutine implementing the differential equations for each species
  subroutine F (nparams, T, c, YDOT)
    implicit none

    integer, intent(in)                                 :: nparams
    double precision, intent(inout)                     :: T
    double precision, dimension(nparams), intent(in)    :: c
    double precision, dimension(nparams), intent(inout) :: YDOT

    double precision, dimension(n_reactions) :: v
    integer :: i, j, temp

    double precision :: R

    Parameter(R=8.3144598D0)
    temp = n_species + 1

    v(:) = preexponential_factor(:) * DEXP(-activation_energy(:)/(R * c(temp)))
    do i=1,n_species
      v(:) = v(:) * ( c(i)**reactions_order_matrix(i,:) )
    end do

    YDOT(:n_species) = 0
    do j=1,n_reactions
      YDOT(:n_species) = YDOT(:n_species) + ( stoichiometric_matrix(:,j) * v(j) )
    end do
    YDOT(:n_species) = YDOT(:n_species) &
                     + ( &
                        flow_entry_for_species(:)*concentration_entry_for_species(:) &
                        - Flow_output * c(:) &
                      ) / Volume_reactor

    YDOT(temp) = (Flow_output/Volume_reactor)*(Temp_reactives - c(temp)) &
               - (Volume_reactor/heat_capacity)*sum(reactions_enthalpy(:)*v(:)) &
               + (kappa/heat_capacity)*(Temp_ambient - c(temp))

  end subroutine F
end module chemical_simulator_with_temp
